﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MultiHelper : MonoBehaviour {

    public TMP_InputField stworzPokoj, dolaczDoPokoju;
    public TextMeshProUGUI stworzInfo, dolaczInfo, tytulLevelu;
    public GameObject MultiManagerPanel;

    [SerializeField]
    private string sceneName;

    [SerializeField]
    private MultiManager multiManager;

    [SerializeField]
    private PlayerController playerController;


    private void Start()
    {
        multiManager = FindObjectOfType<MultiManager>();
        playerController = FindObjectOfType<PlayerController>();
    }

    public void DolaczButton()
    {
        if(dolaczDoPokoju.text == "")
        {
            dolaczInfo.text = "Musisz wpisać jakąś nazwę pokoju!";
        }
        else
        {
            multiManager.RoomName = dolaczDoPokoju.text;
            multiManager.IsHost = false;
            multiManager.JoinRoom(playerController.UserName, playerController.PlayerPoints);
            if (multiManager.RoomOK)
            {
                dolaczInfo.text = multiManager.serverInfo;
                SceneManager.LoadScene(sceneName);
            }
            else
            {
                dolaczInfo.text = multiManager.serverInfo;
            }
        }
    }

    public void StworzButton()
    {
        if (stworzPokoj.text == "")
        {
            stworzInfo.text = "Musisz wpisać jakąś nazwę pokoju!";
        }
        else
        {
            multiManager.RoomName = stworzPokoj.text;
            multiManager.IsHost = true;
            multiManager.CreateRoom(playerController.UserName, playerController.PlayerPoints);
            if(multiManager.RoomOK)
            {
                SceneManager.LoadScene(sceneName);
            }
            else
            {
                stworzInfo.text = multiManager.serverInfo;
            }
        }
    }

    public string SceneName
    {
        get
        {
            return sceneName;
        }
        set
        {
            sceneName = value;
        }
    }

    public void OpenLevel(string name)
    {
        multiManager.LvlName = name;
        tytulLevelu.text = name;
        MultiManagerPanel.SetActive(true);
    }

}
