﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private string userName;

    [SerializeField]
    private int playerPoints;

    public static PlayerController instance;

    void Awake()
    {
        //Application.targetFrameRate = 100;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int PlayerPoints
    {
        get
        {
            return playerPoints;
        }
        set
        {
            playerPoints = value;
        }
    }

    public string UserName
    {
        get
        {
            return userName;
        }
        set
        {
            userName = value;
        }
    }

    public void AddPoints(int points)
    {
        if(playerPoints == 0 && points < 0)
        {
            playerPoints += 0;
        }
        else
        {
            playerPoints += points;
        }
    }

}
    