﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class LevelController : MonoBehaviour {

    public GameObject[] zadaniePanel;
    public Question[] pytanieClass;

    public GameObject finalPanel;

    [SerializeField]
    private double maxLevelTime = 1;

    [SerializeField]
    private MultiManager multiManager;

    [SerializeField]
    private PlayerController playerController;

    public int playersNumber = 1;

    [SerializeField]
    private float timepass = 0;

    [SerializeField]
    private float timeStep = 5;

    [SerializeField]
    private bool initialTimerStarted = false;

    public TextMeshProUGUI timerText;

    #region Start Variables

    public GameObject startPanel;
    public GameObject[] playerPanel = new GameObject[4];
    public TextMeshProUGUI[] playerNameTEXT = new TextMeshProUGUI[4];
    public GameObject gramyButton;

    public TextMeshProUGUI roomName;
    public TextMeshProUGUI levelName;

    #endregion

    #region GameStart

    public TextMeshProUGUI gameStartInfo;

    public DateTime gameStart, counterStart, finishTime;
    
    bool startGAMEBOOL = false;

    #endregion

     void Start () {
        playerController = FindObjectOfType<PlayerController>();
        multiManager = FindObjectOfType<MultiManager>();

        if (!multiManager.IsHost)
        {
            gramyButton.SetActive(false);
        }

        playerPanel[1].SetActive(false);
        playerPanel[2].SetActive(false);
        playerPanel[3].SetActive(false);
        roomName.text = multiManager.RoomName;
        levelName.text = multiManager.LvlName; 

        foreach (GameObject gm in zadaniePanel)
        {
            gm.SetActive(false);
        }

        CheckPlayersNumber();
    }

    void Update()
    {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.name == "O1")
                {
                    zadaniePanel[0].SetActive(true);
                    pytanieClass[0].BetBlocker(playerController.PlayerPoints);
                }

                if (raycastHit.collider.name == "O2")
                {
                    zadaniePanel[1].SetActive(true);
                    pytanieClass[1].BetBlocker(playerController.PlayerPoints);
                }

                if (raycastHit.collider.name == "O3")
                {
                    zadaniePanel[2].SetActive(true);
                    pytanieClass[2].BetBlocker(playerController.PlayerPoints);
                }

                if (raycastHit.collider.name == "O4")
                {
                    zadaniePanel[3].SetActive(true);
                    pytanieClass[3].BetBlocker(playerController.PlayerPoints);
                }
                if (raycastHit.collider.name == "O5")
                {
                    zadaniePanel[4].SetActive(true);
                    pytanieClass[4].BetBlocker(playerController.PlayerPoints);
                }
                if (raycastHit.collider.name == "O6")
                {
                    zadaniePanel[5].SetActive(true);
                    pytanieClass[5].BetBlocker(playerController.PlayerPoints);
                }
                if (raycastHit.collider.name == "O14")
                {
                    zadaniePanel[6].SetActive(true);
                    pytanieClass[6].BetBlocker(playerController.PlayerPoints);
                }

            }
        }

        timepass += Time.deltaTime;
        if(timepass >= timeStep)
        {
            timepass = 0f;
            CheckIfGameStarted();
        }

        if (initialTimerStarted)
        {
            TimeSpan diff = gameStart - DateTime.Now;
            gameStartInfo.text = diff.Minutes.ToString() + " : " + diff.Seconds.ToString();

            if (diff.Seconds <= 0)
            {
                initialTimerStarted = false;
                startPanel.SetActive(false);
                startGAMEBOOL = true;
            }
        }

        if (startGAMEBOOL)
        {
            UpdateTimer();
        }
    }

    public int PlayersNumber
    {
        get
        {
            return playersNumber;
        }
        set
        {
            playersNumber = value;
        }
    }

    public void CheckPlayersNumber()
    {
        multiManager.CheckPlayersNumber();

        playersNumber = multiManager.PlayerNumber;
        ShowPlayers(playersNumber);

        for (int i = 0; i < playersNumber; i++)
        {
            playerNameTEXT[i].text = multiManager.userNames[i];
        }

    }

    public void ShowPlayers(int number)
    {
        if(number == 1)
        {
            playerPanel[0].SetActive(true);
            playerPanel[1].SetActive(false);
            playerPanel[2].SetActive(false);
            playerPanel[3].SetActive(false);
        }
        else if(number == 2)
        {
            playerPanel[0].SetActive(true);
            playerPanel[1].SetActive(true);
            playerPanel[2].SetActive(false);
            playerPanel[3].SetActive(false);

        }
        else if (number == 3)
        {
            playerPanel[0].SetActive(true);
            playerPanel[1].SetActive(true);
            playerPanel[2].SetActive(true);
            playerPanel[3].SetActive(false);

        }
        else if (number == 4)
        {
            playerPanel[0].SetActive(true);
            playerPanel[1].SetActive(true);
            playerPanel[2].SetActive(true);
            playerPanel[3].SetActive(true);
        }
    }

    public void StartGame()
    {
        multiManager.StartGame();
    }

    public void CheckIfGameStarted()
    {
        multiManager.CheckStart();

        if(multiManager.serverInfo == "TURNOFF")
        {
            //Debug.Log("Jeszcze czekam na serwer");
        }
        else
        {
            initialTimerStarted = true;
            counterStart = DateTime.Now;
            gameStart = DateTime.Parse(multiManager.serverInfo);
        }
    }

    public void UpdateTimer()
    {
        finishTime = gameStart.AddMinutes(maxLevelTime);
        TimeSpan dif = finishTime - DateTime.Now;
        timerText.text = dif.Minutes + ":" + dif.Seconds;
        if(dif.Minutes <= 0 && dif.Seconds <= 1)
        {
            finalPanel.SetActive(true);
            startGAMEBOOL = false;
        }
    }

}
