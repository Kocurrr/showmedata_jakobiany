﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class ShowScore : MonoBehaviour {

    public GameObject[] playersPanels = new GameObject[4];
    public TextMeshProUGUI[] playerName = new TextMeshProUGUI[4];
    public TextMeshProUGUI[] playerPoints = new TextMeshProUGUI[4];

    [SerializeField]
    private MultiManager multiManager;

    public DateTime actualTime;

    [SerializeField]
    private bool timeStop = true;

    [SerializeField]
    private int timeStepCheck = 5;

    [SerializeField]
    private string serverInfo;

    [SerializeField]
    private int playerNumber = 1;

    private string GetPointsURL = "http://wojtka.home.pl/Jakobiany/GetPoints.php";



    // Use this for initialization
    void Start () {
        multiManager = FindObjectOfType<MultiManager>();
        GetPoints();
        HidePanels();
    }
	
	// Update is called once per frame
	void Update () {
        if (timeStop)
        {
            actualTime = DateTime.Now;
            timeStop = false;
        }

        TimeSpan diff = DateTime.Now - actualTime;
        if(diff.Seconds >= timeStepCheck)
        {
            timeStop = true;
            GetPoints();
        }
	}

    public void GetPoints()
    {
        StartCoroutine(getPoints());
    }

    IEnumerator getPoints()
    {
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", multiManager.RoomName);

        WWW www = new WWW(GetPointsURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);


        JsonClass myObject = new JsonClass();


        serverInfo = www.text;
        //PlayerNumber = int.Parse(serverInfo);

        myObject = JsonUtility.FromJson<JsonClass>(www.text);

        playerNumber = myObject.no;
        playerName[0].text = myObject.Pl1;
        playerName[1].text = myObject.Pl2;
        playerName[2].text = myObject.Pl3;
        playerName[3].text = myObject.Pl4;

        playerPoints[0].text= myObject.P1.ToString();
        playerPoints[1].text = myObject.P2.ToString();
        playerPoints[2].text = myObject.P3.ToString();
        playerPoints[3].text = myObject.P4.ToString();
        HidePanels();
        yield return www;
    }

    public class JsonClass
    {
        public int no;
        public string Pl1;
        public string Pl2;
        public string Pl3;
        public string Pl4;

        public int P1;
        public int P2;
        public int P3;
        public int P4;
    }

    public void HidePanels()
    {
        switch (playerNumber)
        {
            case 1:
                playersPanels[0].SetActive(true);
                playersPanels[1].SetActive(false);
                playersPanels[2].SetActive(false);
                playersPanels[3].SetActive(false);
                break;
            case 2:
                playersPanels[0].SetActive(true);
                playersPanels[1].SetActive(true);
                playersPanels[2].SetActive(false);
                playersPanels[3].SetActive(false);
                break;
            case 3:
                playersPanels[0].SetActive(true);
                playersPanels[1].SetActive(true);
                playersPanels[2].SetActive(true);
                playersPanels[3].SetActive(false);
                break;
            case 4:
                playersPanels[0].SetActive(true);
                playersPanels[1].SetActive(true);
                playersPanels[2].SetActive(true);
                playersPanels[3].SetActive(true);
                break;
        }
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("MainMenu");
        PlayerController playerCon = FindObjectOfType<PlayerController>();
        Destroy(playerCon);
        Destroy(multiManager);
    }
}
