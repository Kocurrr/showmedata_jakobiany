﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Question : MonoBehaviour {
    
    public TextMeshProUGUI trescTxt;
    public TextMeshProUGUI[] odpTxt = new TextMeshProUGUI[4];
    public Button[] betButtons = new Button[3];
    public Button[] odpButtons = new Button[4];

    [SerializeField]
    private string trescPytania;

    [SerializeField]
    private string podpowiedzPytania;

    [SerializeField]
    private string[] odpowiedz = new string[4];

    [SerializeField]
    private int odpowiedzPoprawna;

    [SerializeField]
    private bool hintTaken = false;

    [SerializeField]
    private PlayerController playerController;

    [SerializeField]
    private int betPoints = 1;

    private string AddPointsURL = "http://wojtka.home.pl/Jakobiany/SetPoints.php";

    private MultiManager multiManager;


    // Use this for initialization
    void Start ()
    {
        trescTxt.text = trescPytania;
        playerController = FindObjectOfType<PlayerController>();
        for (int i = 0; i < 4; i++)
        {
            odpTxt[i].text = odpowiedz[i];
        }
        BetBlocker(playerController.PlayerPoints);
        Bet(1);
        multiManager = FindObjectOfType<MultiManager>();
    }
	
    public void OdpowiedzButton(int ID)
    {
        if(ID == odpowiedzPoprawna)
        {
            if (!hintTaken)
            {
                //super dobra odpowiedź za pierwszym razem
                AddPoints(betPoints);
                BlockAllButtons();
            }
            else
            {
                //tutaj niby plus ale wychodzisz na zero
                AddPoints(betPoints);
                BlockAllButtons();
            }
        }
        else
        {
            if (!hintTaken)
            {
                //pierwszy błąd teraz możesz tylko wyjść na zero
                trescTxt.text = podpowiedzPytania;

                hintTaken = true;
                BlockBetButtons();
                AddPoints(-betPoints);
            }
            else
            {
                //Definitywny koniec pytania
                BlockAllButtons();
            }
        }
    }

    private void BlockAllButtons()
    {
        BlockBetButtons();
        BlockAnswers();
    }

    private void BlockBetButtons()
    {
        foreach (Button btn in betButtons)
        {
            btn.interactable = false;
        }
    }

    private void BlockAnswers()
    {
        foreach (Button btn in odpButtons)
        {
            btn.interactable = false;
        }
    }

    public void BetBlocker(int points)
    {
        if (points < 2)
        {
            betButtons[0].interactable = true;
            betButtons[1].interactable = false;
            betButtons[2].interactable = false;
        }
        else if(points < 3)
        {
            betButtons[0].interactable = true;
            betButtons[1].interactable = true;
            betButtons[2].interactable = false;
        }
        else
        {
            betButtons[0].interactable = true;
            betButtons[1].interactable = true;
            betButtons[2].interactable = true;
        }
    }

    public void Bet(int points)
    {
        if (points == 1)
        {
            betPoints = 1;
            betButtons[0].GetComponent<Image>().color = Color.yellow;
            betButtons[1].GetComponent<Image>().color = Color.white;
            betButtons[2].GetComponent<Image>().color = Color.white;
        }
        else if (points == 2)
        {
            betPoints = 2;
            betButtons[0].GetComponent<Image>().color = Color.white;
            betButtons[1].GetComponent<Image>().color = Color.yellow;
            betButtons[2].GetComponent<Image>().color = Color.white;

        }
        else if (points == 3)
        {
            betPoints = 3;
            betButtons[0].GetComponent<Image>().color = Color.white;
            betButtons[1].GetComponent<Image>().color = Color.white;
            betButtons[2].GetComponent<Image>().color = Color.yellow;

        }
    }

    public void AddPoints(int points)
    {
        StartCoroutine(addPoints(points));
    }

    IEnumerator addPoints(int points)
    {
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", multiManager.RoomName);
        form.AddField("pointsPOST", points);
        form.AddField("namePOST", playerController.UserName);

        WWW www = new WWW(AddPointsURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);

        Debug.Log(www.text);
        yield return www;
    }

}
