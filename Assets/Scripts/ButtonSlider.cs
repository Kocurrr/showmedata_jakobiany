﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSlider : MonoBehaviour {

    public GameObject[] sliderButton;
    public Button rightButton, leftButton;

    [SerializeField]
    private int actutallButton = 0;

    private void Start()
    {
        ChangeButton(0);
    }

    public void ChangeButton(int i)
    {
        actutallButton += i;
        if(actutallButton == 0)
        {
            leftButton.interactable = false;
            rightButton.interactable = true;
            ShowButton();
        }
        else if (actutallButton == sliderButton.Length-1)
        {
            rightButton.interactable = false;
            leftButton.interactable = true;
            ShowButton();
        }
        else
        {
            rightButton.interactable = true;
            leftButton.interactable = true;
            ShowButton();
        }

    }

    private void ShowButton()
    {
        int i = 0;
        foreach(GameObject gm in sliderButton)
        {           
            if (i != actutallButton)
            {
                gm.SetActive(false);
            }
            else
            {
                gm.SetActive(true);
            }

            i++;
        }
    }

}
